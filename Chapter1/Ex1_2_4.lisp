(defun ex (b n)
  (if (= n 0) 1 (* b (expt b (- n 1)))))

(defun e (b n)
  (expt-iter b n 1))

(defun expt-iter (b counter product)
  (if (= counter 0)
      product
      (expt-iter b (- counter 1) (* product b))))

(defun power(x)
  (* x x))

(

(defun fast-expt (b n)
  (cond ((= n 0) 1)
	((even n) (power (fast-expt b (/ n 2))))
	((* b (fast-expt b (- n 1))))))

(defun even (n)
  (= (remainder n 2) 0))
