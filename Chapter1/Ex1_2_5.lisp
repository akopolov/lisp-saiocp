(defun nod (a b)
  (if (= b 0) a (gcd b (rem a b))))

(defun smallest-div (n)
  (find-div n 2))

(defun find-div (n test)
  (cond ((> (square test) n) n)
	((divides test n) test)
	((find-div n (+ test 1)))))

(defun divides (a b)
  (= (rem b a) 0))

(defun square (a)
  (* a a))

(defun prime (n)
  (= n (smallest-div n)))


(defun expmod (base exp m)
  (cond ((= exp 0) 1)
	((even exp)
	 (rem (square (expmod base (/ exp 2) m))
	      m))
	((rem (* base (expmod base (- exp 1) m))
	      m))))

(defun even (n)
  (if (= (rem n 2) 0) T NIL))

(defun fermat (n)
  (defun try-it (a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(defun fast-prime (n times)
  (cond ((= times 0) T)
	((fermat n) (fast-prime n (- times 1)))
	(NIL)))


Упрожнение 1.22

код не работае всязи старого издания
этот вид кода болше не сушествует
(defun time-prime-test (n)
  (print )
  (print n)
  (start-prime-test n (runtime)))

(defun start-prime-test (n start-time)
  (if (prime n)
      (report-prime (- (runtime) start-time))))

(defun report-prime (elapsed-time)
  (print "***")
  (print elapsed-time))


