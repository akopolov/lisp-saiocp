(defun mod (x)
  (cond
    ((> x 0) x)
    ((= x 0) 0)
    ((< x 0) (- x))))

(defun mod_e (x)
  (cond ((< x 0) (- x))
	(x)))

(defun mod_if (x)
  (if (< x 0) (- x) x))
      
