(defun factorial (n)
  (if (= n 1)
      1
      (* n (factorial (- n 1)))))

(defun factorialx (n)
  (fact-iter 1 1 n))

(defun fact-iter (product counter max-count)
  (if (> counter max-count)
      product
      (fact-iter (* product counter)
		 (+ counter 1)
		 max-count)))


(defun fact (n)
  (defun iter (product count)
    (if (> count n)
	product
	(iter (* count product)
	      (+ count 1))))
  (iter 1 1))
