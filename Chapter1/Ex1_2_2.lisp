(defun fib (n)
  (cond ((= n 0) 0)
	((= n 1) 1)
	((+ (fib (- n 1))
	    (fib (- n 2))))))

(defun f (n)
  (fib-iter 1 0 n))

(defun fib-iter (a b count)
  (if (= count 0)
      b
      (fib-iter (+ a b) a (- count 1))))

(defun count-change (amount)
  (cc amount 5))

(defun cc (amount koc)
  (cond ((= amount 0) 1)
	((or (< amount 0) (= koc 0)) 0)
	((+ (cc amount (- koc 1))
	    (cc (- amount (first-d koc)) koc)))))

(defun first-d (koc)
  (cond ((= koc 1) 1)
	((= koc 2) 5)
	((= koc 3) 10)
	((= koc 4) 25)
	((= koc 5) 50)))

