1.9
(defun sa (a b)
  (if  (= a 0)
       b
       (inc (+ (dec a ) b))))

(defun inc (x)
  (+ x 1))

(defun dec (x)
  (- x 1))

(defun sb (a b)
  (if (= a 0)
      b
      (+ (dec a) (inc b))))

1.10
(defun A (x y)
  (cond ((= y 0) 0)
	((= x 0) (* 2 y))
	((= y 1) 2)
	((A (- x 1) (A x (- y 1))))))
