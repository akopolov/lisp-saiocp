1.16 улучшенный способ возведения
(defun fast(n b a)
  (cond ((= n 0) a)
	((= n 1) a)
	((if(= (rem n 2) 0) (fast (/ n 2) (* b b) (* b a))
	   (fast (- n 1) b (* b a))))))

(defun power (b n)
  (fast n b b))

1.17
(defun mult (a b)
  (cond ((= b 0) 0)
	((= a 0) 0)
	((+ a(mult a (- b 1))))))

(defun fast-mult(a b c)
  (cond ((= b 0) 0)
	((= a 0) 0)
	((= a 1) b)
	((= b 1) c)
	((if(= (rem b 2) 0) (fast-mult (+ a a) (/ b 2) (+ a c))
	    (fast-mult a (- b 1) (+ a c))))))

(defun mu (a b)
  (fast-mult a b a))

