(defun square (x)
  (* x x))

(defun sum-of-square (x y)
  (+ (square x) (square y)))

(defun f (a)
  (sum-of-square (+ a 1) (* a 2)))
