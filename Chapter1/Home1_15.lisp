Нахождение приблежонного значения синуса
(defun cub (x)
  (* x x x))

(defun p (x)
  (- (* 3 x) (* 4 (cub x))))

(defun sn (angle)
  (if (not (> (abs angle) 0.1))
      angle
      (p (sin (/ angle 3.0)))))

