(defun square (x)
  (* x x))

(defun average (x y)
  (/ (+ x y) 2))


(defun sq (x)
  (defun good-enough (guess x)
    (< (abs (- (square guess) x)) 0.001))
  (defun improve (guess x)
    (average guess (/ x guess)))
  (defun sqrt-iter (guess x)
    (if (good-enough guess x)
	guess
	(sqrt-iter (improve guess x) x)))
  (sqrt-iter 1.0 x))


(defun s (x)
  (defun good-enough (guess)
    (< (abs (- (square guess) x)) 0.001))
  (defun improve (guess)
    (average guess (/ x guess)))
  (defun sqrt-iter (guess)
    (if (good-enough guess)
	guess
	(sqrt-iter (improve guess))))
  (sqrt-iter 1.0))
