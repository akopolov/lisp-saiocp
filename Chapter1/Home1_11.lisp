
(defun alg(n)
  (cond ((< n 3) n)
	((+ (alg (- n 1)) (alg (- n 2)) (alg (- n 3))))))


(defun fa(n)
  (f-iter 0 1 2 n))

(defun f-iter ( a b c n)
  (if (< n 1) a
      (f-iter b c (+ a b c) (- n 1))))

Треугольник Паскаля

    1
   1 1
  1 2 1
 1 3 3 1
1 4 5 4 1

(defun fact(a n)
  (cond((= n 0) 1)
       ((= n 1) a)
       ((fact (* a n) (- n 1)))))

(defun fac (x)
  (fact 1 x))

(defun pascal (x y)
  (/ (fac y) (* (fac x) (fac (- y x)))))
