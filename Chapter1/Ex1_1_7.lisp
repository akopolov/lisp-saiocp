Квадратный корень методом Ньютона

(defun average (x y)
  (/ (+ x y) 2))

(defun improve (guess x)
  (average guess (/ x guess)))

(defun square (x)
  (* x x))

(defun good-enough (guess x)
  (< (abs (- (square guess) x)) (/ x 1000)))

(defun sqrt-iter (guess x)
  (if (good-enough guess x)
      guess
      (sqrt-iter (improve guess x) x)))

(defun sq (x)
  (sqrt-iter 1.0 x))

ХЗ но пишет что overflow 

(defun new-if (predicate then-cl else-cl)
  (cond (predicate then-cl)
	(else-cl)))

(defun test(guess x)
  (new-if (good-enough guess x)
	  guess
	  (test (improve guess x) x)))

Куб

(defun power (x)
  (* x x x))

(defun kub (x)
  (kub-iter 1.0 x))

(defun kub-iter (guess x)
  (if (kub-good guess x)
      guess
      (kub-iter (improve-kub guess x) x)))

(defun kub-good (guess x)
  (< (abs (- (power guess) x)) (/ x 10000)))

(defun kub-aver (x y)
  (/ (+ x y) 3))

(defun improve-kub (guess x)
  (kub-aver (/ x (square guess)) (* 2 guess)))

